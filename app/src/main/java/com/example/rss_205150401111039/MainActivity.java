package com.example.rss_205150401111039;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;


public class MainActivity extends AppCompatActivity {

    final static String source = "https://medium.com/feed/tag/programming";
    TextView id;

    RecyclerView rv;

    private ArrayList<String> dataSet;
    private NewsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id = findViewById(R.id.namanim);

        rv = findViewById(R.id.rv1);
        rv.setLayoutManager(new LinearLayoutManager(this));

        //coba ke recycleview
        AsyncExample task2=new AsyncExample(rv);
        task2.execute();
    }


    //
    public class AsyncExample extends AsyncTask<Void, Void, ArrayList<News>> {
        ProgressDialog progress = new ProgressDialog(MainActivity.this);

        RecyclerView rv;
        public AsyncExample(RecyclerView rv){
            this.rv = rv;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            progress.setMessage("Memuat Data");
            progress.show();
        }

        @Override
        protected ArrayList<News> doInBackground(Void... voids) {
            RssParser parser = new RssParser();
            try {
                String xml = parser.loadRssFromUrl(source);
                ArrayList<News>title = parser.parseRssFromUrl(xml);
                return title;
            } catch (IOException e){
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<News> s){
            super.onPostExecute(s);

        //coba ke recycleview
            adapter = new NewsAdapter(MainActivity.this, s);
            rv.setAdapter(adapter);
            progress.dismiss();
            adapter.notifyDataSetChanged();

        }
        }
    }


